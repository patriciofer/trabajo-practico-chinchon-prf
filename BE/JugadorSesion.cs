﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class JugadorSesion
    {
        private Jugador jugador;
        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }


        private static JugadorSesion instancia;
        protected JugadorSesion()
        {

        }
        public static JugadorSesion Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new JugadorSesion();
                }
                return instancia;
            }
        }
    }
}
