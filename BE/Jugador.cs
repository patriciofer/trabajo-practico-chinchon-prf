﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Jugador
    {

        private int id;
                
        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }


        private string usuario;

           public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }


        private string clave;

        public string Clave
        {
            get { return clave; }
            set { clave = value; }
        }


        private List<Carta> cartas;

        public List<Carta> Cartas
        {
            get { return cartas; }
            set { cartas = value; }
        }

        private List<Carta> cartasSobrantes;

        public List<Carta> CartasSobrantes
        {
            get { return cartasSobrantes; }
            set { cartasSobrantes = value; }
        }



        public Jugador ()
        {

            cartas = new List<Carta>();

        }

        public string NombreCompleto
        {
            get
            {
                return string.Format("{0} {1}", nombre, apellido);
            }
        }


        private int cartasSumadas;

        public int CartasSumadas
        {
            get { return cartasSumadas; }
            set { cartasSumadas = value; }
        }



        private int partidasGanadas;

        public int PartidasGanadas
        {
            get { return partidasGanadas; }
            set { partidasGanadas = value; }
        }

        private int partidasPerdidas;

        public int PartidasPerdidas
        {
            get { return partidasPerdidas; }
            set { partidasPerdidas = value; }
        }

        private int partidasEmpatadas;

        public int PartidasEmpatadas
        {
            get { return partidasEmpatadas; }
            set { partidasEmpatadas = value; }
        }

        private int totalTiempoJugado;

        public int TotalTiempoJugado
        {
            get { return totalTiempoJugado; }
            set { totalTiempoJugado = value; }
        }

        private int partidasJugadas;

        public int PartidasJugadas
        {
            get { return partidasJugadas; }
            set { partidasJugadas = value; }
        }

        private DateTime fechaHoraInicioPartida;

       // [Browsable(false)]
        public DateTime FechaHoraInicioPartida
        {
            get { return fechaHoraInicioPartida; }
            set { fechaHoraInicioPartida = value; }
        }

        private DateTime fechaHoraFinPartida;

        //[Browsable(false)]
        public DateTime FechaHoraFinPartida
        {
            get { return fechaHoraFinPartida; }
            set { fechaHoraFinPartida = value; }
        }

        public double PromedioVictorias
        {
            get { return PartidasGanadas > 0 ? partidasJugadas / partidasGanadas : 0; }
        }

        private string tipoMovimiento;

        public string TipoMovimiento
        {
            get { return tipoMovimiento; }
            set { tipoMovimiento = value; }
        }


    }
}
