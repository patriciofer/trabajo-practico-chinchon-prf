﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Turno
    {
        List<Carta> cartasSeleccionadas = new List<Carta>();

        public List<Carta> CartasSeleccionadas
        {
            get { return cartasSeleccionadas; }
        }

        public void AgregarCarta(Carta carta)
        {
            cartasSeleccionadas.Clear();
            cartasSeleccionadas.Add(carta);
        }
    }
}