﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
	public class CortarMesaRespuesta
	{
		private bool puedeCortarMesa;

		public bool PuedeCortarMesa
		{
			get { return puedeCortarMesa; }
			set { puedeCortarMesa = value; }
		}

		private List<Carta> cartasJugadorSobrantes;

		public List<Carta> CartasJugadorSobrantes
		{
			get { return cartasJugadorSobrantes; }
			set { cartasJugadorSobrantes = value; }
		}
	}
}
