﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public  class Carta
    {
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string palo;

        public string Palo
        {
            get { return palo; }
            set { palo = value; }
        }

        public string RutaImagen
        {
            get
            {
                return @"img\cartas\" + palo + @"\" + numero + ".jpg";
            }
        }
    }
}
