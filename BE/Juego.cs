﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Juego
    {


        private List <Jugador> jugadores;

        public List <Jugador> Jugadores
        {
            get { return jugadores; }
            set { jugadores = value; }
        }


        private List <Carta> cartas;

        public List <Carta> Cartas  
        {
            get { return cartas; }
            set { cartas = value; }
        }

        private List<Carta> cartasVisible;

        public List<Carta> CartasVisible
        {
            get { return cartasVisible; }
            set { cartasVisible = value; }
        }


        private List<Carta> cartasNoVisible;

        public List<Carta> CartasNoVisible
        {
            get { return cartasNoVisible; }
            set { cartasNoVisible = value; }
        }


        public Juego ()
        {

            jugadores = new List<Jugador>();
            cartas = new List<Carta>();

            CartasVisible = new List<Carta>();
            CartasNoVisible = new List<Carta>();
        }


        private int indice = 0;

        public void CambiarTurnoJugador()
        {
            indice = (indice + 1) % jugadores.Count();
        }

        public void CambiarTurnoJugador(int indice)
        {
            this.indice = indice;
        }

        public Jugador JugadorActivo
        {
            get
            {
                return jugadores[indice];
            }
        }


    }






}
