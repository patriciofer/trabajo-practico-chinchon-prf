﻿using System;

namespace BE
{
    public class Bitacora
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private DateTime fechaHora;

        public DateTime FechaHora
        {
            get { return fechaHora; }
            set { fechaHora = value; }
        }

        private TipoBitacoraItem tipoBitacoraItem;

        public TipoBitacoraItem TipoBitacoraItem
        {
            get { return tipoBitacoraItem; }
            set { tipoBitacoraItem = value; }
        }

        private Jugador jugador;

        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }

        

    }
}

