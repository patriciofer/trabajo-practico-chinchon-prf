﻿namespace GUI
{
    partial class CartaUserControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.CartaPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.CartaPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CartaPictureBox
            // 
            this.CartaPictureBox.Location = new System.Drawing.Point(0, 0);
            this.CartaPictureBox.Name = "CartaPictureBox";
            this.CartaPictureBox.Size = new System.Drawing.Size(100, 139);
            this.CartaPictureBox.TabIndex = 0;
            this.CartaPictureBox.TabStop = false;
            this.CartaPictureBox.Click += new System.EventHandler(this.CartaPictureBox_Click);
            // 
            // CartaUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CartaPictureBox);
            this.Name = "CartaUserControl";
            this.Size = new System.Drawing.Size(100, 139);
            ((System.ComponentModel.ISupportInitialize)(this.CartaPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox CartaPictureBox;
    }
}
