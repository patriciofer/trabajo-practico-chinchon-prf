﻿namespace GUI
{
    partial class FrmBk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBk = new System.Windows.Forms.Button();
            this.lstbk = new System.Windows.Forms.ListBox();
            this.btnrestore = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBk
            // 
            this.btnBk.Location = new System.Drawing.Point(41, 27);
            this.btnBk.Name = "btnBk";
            this.btnBk.Size = new System.Drawing.Size(271, 23);
            this.btnBk.TabIndex = 0;
            this.btnBk.Text = "Realizar Bk de Base de datos";
            this.btnBk.UseVisualStyleBackColor = true;
            this.btnBk.Click += new System.EventHandler(this.btnBk_Click);
            // 
            // lstbk
            // 
            this.lstbk.FormattingEnabled = true;
            this.lstbk.Location = new System.Drawing.Point(41, 71);
            this.lstbk.Name = "lstbk";
            this.lstbk.Size = new System.Drawing.Size(271, 199);
            this.lstbk.TabIndex = 1;
            // 
            // btnrestore
            // 
            this.btnrestore.Location = new System.Drawing.Point(41, 277);
            this.btnrestore.Name = "btnrestore";
            this.btnrestore.Size = new System.Drawing.Size(271, 23);
            this.btnrestore.TabIndex = 2;
            this.btnrestore.Text = "Realizar Restore";
            this.btnrestore.UseVisualStyleBackColor = true;
            this.btnrestore.Click += new System.EventHandler(this.btnrestore_Click);
            // 
            // FrmBk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 371);
            this.Controls.Add(this.btnrestore);
            this.Controls.Add(this.lstbk);
            this.Controls.Add(this.btnBk);
            this.Name = "FrmBk";
            this.Text = "FrmBk";
            this.Load += new System.EventHandler(this.FrmBk_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBk;
        private System.Windows.Forms.ListBox lstbk;
        private System.Windows.Forms.Button btnrestore;
    }
}