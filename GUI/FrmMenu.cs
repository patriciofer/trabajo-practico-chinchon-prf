﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace GUI
{
    public partial class FrmMenu : Form
    {


        private GestorJugador gestorJugador = new GestorJugador();
        private List<Jugador> jugadoresLogueados = new List<Jugador>();
        private GestorBitacora gestorBitacora = new GestorBitacora();

        public FrmMenu()
        {
            InitializeComponent();
        }

        private void bindJugadoresLogueados()
        {
            DgvUsuariosLogueados.DataSource = null;
            DgvUsuariosLogueados.DataSource = jugadoresLogueados;
        }

        public void NewBindJugadoresLogueados()
        {
            jugadoresLogueados.Clear();
            bindJugadoresLogueados();
        }


        private void bindJugadoresRegistrados()
        {
            DgvUsuariosRegistrados.DataSource = null;
            DgvUsuariosRegistrados.DataSource = gestorJugador.GetAll();
        }


        private bool validateLogin()
        {
            if (jugadoresLogueados.Count > 4)
            {
                MessageBox.Show("No pueden jugar más de 4 jugadores", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (TxtUsuarioLogin.Text == "")
            {
                MessageBox.Show("Ingrese su usuario", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtUsuarioLogin.Focus();
                return false;
            }

            if (TxtPasswordLogin.Text == "")
            {
                MessageBox.Show("Ingrese su usuario", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtPasswordLogin.Focus();
                return false;
            }

            if (gestorJugador.CheckPlayerByCredentials(new Jugador() { Usuario = TxtUsuarioLogin.Text, Clave = TxtPasswordLogin.Text }) <= 0)
            {
                MessageBox.Show("Usuario inexistente. Vuelva intentar", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if ((from Jugador j in jugadoresLogueados where j.Usuario == TxtUsuarioLogin.Text && j.Clave == TxtPasswordLogin.Text select j).ToList().Count > 0)
            {
                MessageBox.Show("Jugador ya logueado", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private bool validateRegistrar()
        {
            if (TxtNombre.Text == "")
            {
                MessageBox.Show("Ingrese su nombre", "Nombre", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtNombre.Focus();
                return false;
            }

            if (TxtApellido.Text == "")
            {
                MessageBox.Show("Ingrese su apellido", "Apellido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtApellido.Focus();
                return false;
            }

            if (TxtUsuarioRegistrar.Text == "")
            {
                MessageBox.Show("Ingrese su usuario", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtUsuarioRegistrar.Focus();
                return false;
            }

            if (TxtPasswordRegistrar.Text == "")
            {
                MessageBox.Show("Ingrese su usuario", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtPasswordRegistrar.Focus();
                return false;
            }

            if (gestorJugador.CheckPlayerByCredentials(new Jugador() { Usuario = TxtUsuarioRegistrar.Text }) > 0)
            {
                MessageBox.Show("Usuario existente. Ingrese otro usuario", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                TxtUsuarioRegistrar.Focus();
                return false;
            }

            return true;
        }

        private void ClearLoginFields()
        {
            TxtUsuarioLogin.Clear();
            TxtPasswordLogin.Clear();
            TxtUsuarioLogin.Focus();
        }


        private void ClearSignInFields()
        {
            TxtNombre.Clear();
            TxtApellido.Clear();
            TxtUsuarioRegistrar.Clear();
            TxtPasswordRegistrar.Clear();
            TxtNombre.Focus();
        }


  


        private void FrmMenu_Load(object sender, EventArgs e)
        {
            btnComenzarJuego.Enabled = false;
            bindJugadoresRegistrados();
        }

        
        private void btnRegistro_Click(object sender, EventArgs e)
        {
            if (validateRegistrar())
            {
                Jugador j = new Jugador();
                j.Nombre = TxtNombre.Text;
                j.Apellido = TxtApellido.Text;
                j.Usuario = TxtUsuarioRegistrar.Text;
                j.Clave = TxtPasswordRegistrar.Text;
                j.PartidasGanadas = 0;
                j.PartidasPerdidas = 0;
                j.PartidasEmpatadas = 0;
                j.TotalTiempoJugado = 0;
                j.PartidasJugadas = 0;
                gestorJugador.Create(j);
                bindJugadoresRegistrados();
                ClearSignInFields();
            }

        }

        private void btniniciarS_Click(object sender, EventArgs e)
        {
            if (validateLogin())
            {
                Jugador jugador = gestorJugador.FindPlayerByCredentials(new Jugador() { Usuario = TxtUsuarioLogin.Text, Clave = TxtPasswordLogin.Text });
                jugadoresLogueados.Add(jugador);
                bindJugadoresLogueados();
                if (jugadoresLogueados.Count > 3)
                    btnComenzarJuego.Enabled = true;
                gestorBitacora.LogIniciarSesion(jugador);
            }
            ClearLoginFields();
        }

        private void btnComenzarJuego_Click(object sender, EventArgs e)
        {
            FrmMesa frm = new FrmMesa();
            frm.Juego.Jugadores = new List<Jugador>(jugadoresLogueados);
            gestorBitacora.LogComenzarPartida(jugadoresLogueados);
            this.Hide();
            frm.Show();
        }
    }
}
