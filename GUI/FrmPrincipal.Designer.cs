﻿namespace GUI
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.registroCominezoJuegoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroComienzoDeJuegoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backUpRestoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimientosXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroCominezoJuegoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // registroCominezoJuegoToolStripMenuItem
            // 
            this.registroCominezoJuegoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroComienzoDeJuegoToolStripMenuItem,
            this.backUpRestoreToolStripMenuItem,
            this.movimientosXMLToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.registroCominezoJuegoToolStripMenuItem.Name = "registroCominezoJuegoToolStripMenuItem";
            this.registroCominezoJuegoToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.registroCominezoJuegoToolStripMenuItem.Text = "Menu de Juego";
            // 
            // registroComienzoDeJuegoToolStripMenuItem
            // 
            this.registroComienzoDeJuegoToolStripMenuItem.Name = "registroComienzoDeJuegoToolStripMenuItem";
            this.registroComienzoDeJuegoToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.registroComienzoDeJuegoToolStripMenuItem.Text = "Registro/Comienzo de Juego";
            this.registroComienzoDeJuegoToolStripMenuItem.Click += new System.EventHandler(this.registroComienzoDeJuegoToolStripMenuItem_Click);
            // 
            // backUpRestoreToolStripMenuItem
            // 
            this.backUpRestoreToolStripMenuItem.Name = "backUpRestoreToolStripMenuItem";
            this.backUpRestoreToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.backUpRestoreToolStripMenuItem.Text = "BackUp/Restore";
            this.backUpRestoreToolStripMenuItem.Click += new System.EventHandler(this.backUpRestoreToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // movimientosXMLToolStripMenuItem
            // 
            this.movimientosXMLToolStripMenuItem.Name = "movimientosXMLToolStripMenuItem";
            this.movimientosXMLToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.movimientosXMLToolStripMenuItem.Text = "Movimientos XML";
            this.movimientosXMLToolStripMenuItem.Click += new System.EventHandler(this.movimientosXMLToolStripMenuItem_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipal";
            this.Text = "FrmPrincipal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem registroCominezoJuegoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroComienzoDeJuegoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backUpRestoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimientosXMLToolStripMenuItem;
    }
}