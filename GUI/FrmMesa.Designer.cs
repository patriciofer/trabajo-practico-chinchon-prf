﻿namespace GUI
{
    partial class FrmMesa
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTomarCarta = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.cartaUserControlMazoNoVisible = new GUI.CartaUserControl();
            this.cartaUserControlMazo = new GUI.CartaUserControl();
            this.cartaUserControl7 = new GUI.CartaUserControl();
            this.cartaUserControl6 = new GUI.CartaUserControl();
            this.cartaUserControl5 = new GUI.CartaUserControl();
            this.cartaUserControl4 = new GUI.CartaUserControl();
            this.cartaUserControl3 = new GUI.CartaUserControl();
            this.cartaUserControl2 = new GUI.CartaUserControl();
            this.cartaUserControl1 = new GUI.CartaUserControl();
            this.LblJugadorActivo = new System.Windows.Forms.Label();
            this.btnCortarMeza = new System.Windows.Forms.Button();
            this.GrillaJugadores = new System.Windows.Forms.DataGridView();
            this.btnReiniciarPartida = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.GrillaJugadores)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTomarCarta
            // 
            this.btnTomarCarta.Location = new System.Drawing.Point(785, 214);
            this.btnTomarCarta.Name = "btnTomarCarta";
            this.btnTomarCarta.Size = new System.Drawing.Size(156, 23);
            this.btnTomarCarta.TabIndex = 8;
            this.btnTomarCarta.Text = "Tomar Carta Mazo Visible";
            this.btnTomarCarta.UseVisualStyleBackColor = true;
            this.btnTomarCarta.Click += new System.EventHandler(this.btnTomarCarta_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Jugador 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(94, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Jugador 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(176, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Jugador 3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(266, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 12;
            this.button4.Text = "Jugador 4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(989, 214);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(160, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Tomar Carta Mazo No Visible";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // cartaUserControlMazoNoVisible
            // 
            this.cartaUserControlMazoNoVisible.Carta = null;
            this.cartaUserControlMazoNoVisible.Location = new System.Drawing.Point(1020, 55);
            this.cartaUserControlMazoNoVisible.Name = "cartaUserControlMazoNoVisible";
            this.cartaUserControlMazoNoVisible.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControlMazoNoVisible.TabIndex = 13;
            this.cartaUserControlMazoNoVisible.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControlMazo
            // 
            this.cartaUserControlMazo.Carta = null;
            this.cartaUserControlMazo.Location = new System.Drawing.Point(816, 55);
            this.cartaUserControlMazo.Name = "cartaUserControlMazo";
            this.cartaUserControlMazo.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControlMazo.TabIndex = 7;
            this.cartaUserControlMazo.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl7
            // 
            this.cartaUserControl7.Carta = null;
            this.cartaUserControl7.Location = new System.Drawing.Point(388, 214);
            this.cartaUserControl7.Name = "cartaUserControl7";
            this.cartaUserControl7.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl7.TabIndex = 6;
            this.cartaUserControl7.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl6
            // 
            this.cartaUserControl6.Carta = null;
            this.cartaUserControl6.Location = new System.Drawing.Point(282, 214);
            this.cartaUserControl6.Name = "cartaUserControl6";
            this.cartaUserControl6.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl6.TabIndex = 5;
            this.cartaUserControl6.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl5
            // 
            this.cartaUserControl5.Carta = null;
            this.cartaUserControl5.Location = new System.Drawing.Point(176, 214);
            this.cartaUserControl5.Name = "cartaUserControl5";
            this.cartaUserControl5.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl5.TabIndex = 4;
            this.cartaUserControl5.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl4
            // 
            this.cartaUserControl4.Carta = null;
            this.cartaUserControl4.Location = new System.Drawing.Point(69, 214);
            this.cartaUserControl4.Name = "cartaUserControl4";
            this.cartaUserControl4.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl4.TabIndex = 3;
            this.cartaUserControl4.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl3
            // 
            this.cartaUserControl3.Carta = null;
            this.cartaUserControl3.Location = new System.Drawing.Point(282, 69);
            this.cartaUserControl3.Name = "cartaUserControl3";
            this.cartaUserControl3.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl3.TabIndex = 2;
            this.cartaUserControl3.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl2
            // 
            this.cartaUserControl2.Carta = null;
            this.cartaUserControl2.Location = new System.Drawing.Point(175, 69);
            this.cartaUserControl2.Name = "cartaUserControl2";
            this.cartaUserControl2.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl2.TabIndex = 1;
            this.cartaUserControl2.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // cartaUserControl1
            // 
            this.cartaUserControl1.Carta = null;
            this.cartaUserControl1.Location = new System.Drawing.Point(69, 69);
            this.cartaUserControl1.Name = "cartaUserControl1";
            this.cartaUserControl1.Size = new System.Drawing.Size(100, 139);
            this.cartaUserControl1.TabIndex = 0;
            this.cartaUserControl1.eventClick += new GUI.CartaUserControl.delClick(this.Carta_Click);
            // 
            // LblJugadorActivo
            // 
            this.LblJugadorActivo.AutoSize = true;
            this.LblJugadorActivo.Location = new System.Drawing.Point(346, 13);
            this.LblJugadorActivo.Name = "LblJugadorActivo";
            this.LblJugadorActivo.Size = new System.Drawing.Size(78, 13);
            this.LblJugadorActivo.TabIndex = 15;
            this.LblJugadorActivo.Text = "Jugador Activo";
            // 
            // btnCortarMeza
            // 
            this.btnCortarMeza.Location = new System.Drawing.Point(51, 417);
            this.btnCortarMeza.Name = "btnCortarMeza";
            this.btnCortarMeza.Size = new System.Drawing.Size(75, 23);
            this.btnCortarMeza.TabIndex = 16;
            this.btnCortarMeza.Text = "Cortar Meza";
            this.btnCortarMeza.UseVisualStyleBackColor = true;
            this.btnCortarMeza.Click += new System.EventHandler(this.btnCortarMeza_Click);
            // 
            // GrillaJugadores
            // 
            this.GrillaJugadores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrillaJugadores.Location = new System.Drawing.Point(546, 355);
            this.GrillaJugadores.Name = "GrillaJugadores";
            this.GrillaJugadores.Size = new System.Drawing.Size(790, 362);
            this.GrillaJugadores.TabIndex = 17;
            // 
            // btnReiniciarPartida
            // 
            this.btnReiniciarPartida.Location = new System.Drawing.Point(141, 417);
            this.btnReiniciarPartida.Name = "btnReiniciarPartida";
            this.btnReiniciarPartida.Size = new System.Drawing.Size(110, 23);
            this.btnReiniciarPartida.TabIndex = 18;
            this.btnReiniciarPartida.Text = "Reiniciar Partida";
            this.btnReiniciarPartida.UseVisualStyleBackColor = true;
            this.btnReiniciarPartida.Click += new System.EventHandler(this.btnReiniciarPartida_Click);
            // 
            // FrmMesa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(1348, 729);
            this.Controls.Add(this.btnReiniciarPartida);
            this.Controls.Add(this.GrillaJugadores);
            this.Controls.Add(this.btnCortarMeza);
            this.Controls.Add(this.LblJugadorActivo);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.cartaUserControlMazoNoVisible);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnTomarCarta);
            this.Controls.Add(this.cartaUserControlMazo);
            this.Controls.Add(this.cartaUserControl7);
            this.Controls.Add(this.cartaUserControl6);
            this.Controls.Add(this.cartaUserControl5);
            this.Controls.Add(this.cartaUserControl4);
            this.Controls.Add(this.cartaUserControl3);
            this.Controls.Add(this.cartaUserControl2);
            this.Controls.Add(this.cartaUserControl1);
            this.Name = "FrmMesa";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrillaJugadores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CartaUserControl cartaUserControl1;
        private CartaUserControl cartaUserControl2;
        private CartaUserControl cartaUserControl3;
        private CartaUserControl cartaUserControl4;
        private CartaUserControl cartaUserControl5;
        private CartaUserControl cartaUserControl6;
        private CartaUserControl cartaUserControl7;
        private CartaUserControl cartaUserControlMazo;
        private System.Windows.Forms.Button btnTomarCarta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private CartaUserControl cartaUserControlMazoNoVisible;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label LblJugadorActivo;
        private System.Windows.Forms.Button btnCortarMeza;
        private System.Windows.Forms.DataGridView GrillaJugadores;
        private System.Windows.Forms.Button btnReiniciarPartida;
    }
}

