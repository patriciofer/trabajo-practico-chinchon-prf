﻿namespace GUI
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btniniciarS = new System.Windows.Forms.Button();
            this.TxtPasswordLogin = new System.Windows.Forms.TextBox();
            this.TxtUsuarioLogin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRegistro = new System.Windows.Forms.Button();
            this.TxtApellido = new System.Windows.Forms.TextBox();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPasswordRegistrar = new System.Windows.Forms.TextBox();
            this.TxtUsuarioRegistrar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DgvUsuariosLogueados = new System.Windows.Forms.DataGridView();
            this.DgvUsuariosRegistrados = new System.Windows.Forms.DataGridView();
            this.btnComenzarJuego = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvUsuariosLogueados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvUsuariosRegistrados)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btniniciarS);
            this.groupBox1.Controls.Add(this.TxtPasswordLogin);
            this.groupBox1.Controls.Add(this.TxtUsuarioLogin);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 119);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Login";
            // 
            // btniniciarS
            // 
            this.btniniciarS.Location = new System.Drawing.Point(13, 90);
            this.btniniciarS.Name = "btniniciarS";
            this.btniniciarS.Size = new System.Drawing.Size(93, 23);
            this.btniniciarS.TabIndex = 3;
            this.btniniciarS.Text = "Iniciar Sesion";
            this.btniniciarS.UseVisualStyleBackColor = true;
            this.btniniciarS.Click += new System.EventHandler(this.btniniciarS_Click);
            // 
            // TxtPasswordLogin
            // 
            this.TxtPasswordLogin.Location = new System.Drawing.Point(71, 52);
            this.TxtPasswordLogin.Name = "TxtPasswordLogin";
            this.TxtPasswordLogin.PasswordChar = '*';
            this.TxtPasswordLogin.Size = new System.Drawing.Size(183, 20);
            this.TxtPasswordLogin.TabIndex = 1;
            // 
            // TxtUsuarioLogin
            // 
            this.TxtUsuarioLogin.Location = new System.Drawing.Point(71, 16);
            this.TxtUsuarioLogin.Name = "TxtUsuarioLogin";
            this.TxtUsuarioLogin.Size = new System.Drawing.Size(183, 20);
            this.TxtUsuarioLogin.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuario:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRegistro);
            this.groupBox2.Controls.Add(this.TxtApellido);
            this.groupBox2.Controls.Add(this.TxtNombre);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TxtPasswordRegistrar);
            this.groupBox2.Controls.Add(this.TxtUsuarioRegistrar);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(3, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(501, 126);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Registrase";
            // 
            // btnRegistro
            // 
            this.btnRegistro.Location = new System.Drawing.Point(24, 90);
            this.btnRegistro.Name = "btnRegistro";
            this.btnRegistro.Size = new System.Drawing.Size(75, 23);
            this.btnRegistro.TabIndex = 10;
            this.btnRegistro.Text = "Registro";
            this.btnRegistro.UseVisualStyleBackColor = true;
            this.btnRegistro.Click += new System.EventHandler(this.btnRegistro_Click);
            // 
            // TxtApellido
            // 
            this.TxtApellido.Location = new System.Drawing.Point(66, 55);
            this.TxtApellido.Name = "TxtApellido";
            this.TxtApellido.Size = new System.Drawing.Size(183, 20);
            this.TxtApellido.TabIndex = 4;
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(66, 19);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(183, 20);
            this.TxtNombre.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Apellido:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Nombre:";
            // 
            // TxtPasswordRegistrar
            // 
            this.TxtPasswordRegistrar.Location = new System.Drawing.Point(312, 55);
            this.TxtPasswordRegistrar.Name = "TxtPasswordRegistrar";
            this.TxtPasswordRegistrar.PasswordChar = '*';
            this.TxtPasswordRegistrar.Size = new System.Drawing.Size(183, 20);
            this.TxtPasswordRegistrar.TabIndex = 6;
            // 
            // TxtUsuarioRegistrar
            // 
            this.TxtUsuarioRegistrar.Location = new System.Drawing.Point(312, 19);
            this.TxtUsuarioRegistrar.Name = "TxtUsuarioRegistrar";
            this.TxtUsuarioRegistrar.Size = new System.Drawing.Size(183, 20);
            this.TxtUsuarioRegistrar.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(253, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(262, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Usuario:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(294, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Usuarios Logueados";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(510, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Usuarios Registrados";
            // 
            // DgvUsuariosLogueados
            // 
            this.DgvUsuariosLogueados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvUsuariosLogueados.Location = new System.Drawing.Point(297, 171);
            this.DgvUsuariosLogueados.Name = "DgvUsuariosLogueados";
            this.DgvUsuariosLogueados.Size = new System.Drawing.Size(766, 92);
            this.DgvUsuariosLogueados.TabIndex = 7;
            // 
            // DgvUsuariosRegistrados
            // 
            this.DgvUsuariosRegistrados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvUsuariosRegistrados.Location = new System.Drawing.Point(510, 34);
            this.DgvUsuariosRegistrados.Name = "DgvUsuariosRegistrados";
            this.DgvUsuariosRegistrados.Size = new System.Drawing.Size(553, 104);
            this.DgvUsuariosRegistrados.TabIndex = 6;
            // 
            // btnComenzarJuego
            // 
            this.btnComenzarJuego.Location = new System.Drawing.Point(12, 303);
            this.btnComenzarJuego.Name = "btnComenzarJuego";
            this.btnComenzarJuego.Size = new System.Drawing.Size(226, 23);
            this.btnComenzarJuego.TabIndex = 11;
            this.btnComenzarJuego.Text = "Comenzar Juego";
            this.btnComenzarJuego.UseVisualStyleBackColor = true;
            this.btnComenzarJuego.Click += new System.EventHandler(this.btnComenzarJuego_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 354);
            this.Controls.Add(this.btnComenzarJuego);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DgvUsuariosLogueados);
            this.Controls.Add(this.DgvUsuariosRegistrados);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmMenu";
            this.Text = "FrmMenu";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvUsuariosLogueados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvUsuariosRegistrados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TxtPasswordLogin;
        private System.Windows.Forms.TextBox TxtUsuarioLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtApellido;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtPasswordRegistrar;
        private System.Windows.Forms.TextBox TxtUsuarioRegistrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView DgvUsuariosLogueados;
        private System.Windows.Forms.DataGridView DgvUsuariosRegistrados;
        private System.Windows.Forms.Button btnRegistro;
        private System.Windows.Forms.Button btniniciarS;
        private System.Windows.Forms.Button btnComenzarJuego;
    }
}