﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void registroComienzoDeJuegoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMenu frmMenu = new FrmMenu();

            frmMenu.Show();
        }

        private void backUpRestoreToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FrmBk frmBackUpRestore = new FrmBk();
            frmBackUpRestore.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }

        private void movimientosXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmXml frmXml  = new FrmXml();
            frmXml.Show();
        }
    }
}
