﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;

namespace GUI
{
    public partial class CartaUserControl : UserControl
    {
        public delegate void delClick(CartaUserControl ucCarta);
        public event delClick eventClick;

        private Carta carta;

        public Carta Carta
        {
            get { return carta; }
            set
            {
                carta = value;
                if (carta == null)
                    CartaPictureBox.Image = null;
                else
                    CartaPictureBox.Image = Image.FromFile(carta.RutaImagen);
            }
        }

        public CartaUserControl()
        {
            InitializeComponent();
        }

        

        private void CartaPictureBox_Click(object sender, EventArgs e)
        {
            this.eventClick(this);

        }
    }
}
