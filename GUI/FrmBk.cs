﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace GUI
{
    public partial class FrmBk : Form
    {
        public FrmBk()
        {
            InitializeComponent();
        }

        private void btnBk_Click(object sender, EventArgs e)
        {
            GestorBackup.RealizarBK();
            MessageBox.Show("Se ha realizado un backup de la BD.");
            CargarGvBackups();
        }


        private void CargarGvBackups()
        {
            lstbk.DataSource = null;
            lstbk.DataSource = GestorBackup.GetBkps();
        }


        private void FrmBk_Load(object sender, EventArgs e)
        {
            CargarGvBackups();
        }

        private void btnrestore_Click(object sender, EventArgs e)
        {
            string nombreArchivo = lstbk.SelectedItem.ToString();

            GestorBackup.RealizarRestore(nombreArchivo);

            MessageBox.Show("Se realizo el restore de la base de datos exitosamente");
        }
    }
}
