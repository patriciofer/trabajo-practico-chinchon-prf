﻿using BE;
using BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GUI
{
	public partial class FrmMesa : Form
    {
        BLL.GestorJuego GestorJuego = new GestorJuego();
        BLL.GestorJugador GestorJugador = new GestorJugador();
        BLL.GestorBitacora GestorBitacora = new GestorBitacora();
        Juego juego = new Juego();

        public Juego Juego
        {
            get
            {
                return juego;
            }
        }

        Turno turno = new Turno();
        public FrmMesa()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GestorJuego.IniciarJuego(juego);

            foreach (Jugador j in juego.Jugadores)
            {
               
                j.FechaHoraInicioPartida = DateTime.Now;
                GestorJugador.Edit(j);
            }


            mostrarCartasVisibles();

            mostrarCartasNoVisibles();

         

            MostrarJugadorActivo();
        }

        private List<CartaUserControl> GetCartasUserControl(int ini, int fin)
        {
            return (from Control uc in this.Controls
                    where uc is CartaUserControl && uc.TabIndex >= ini && uc.TabIndex <= fin
                    orderby uc.TabIndex ascending
                    select (CartaUserControl)uc).ToList();
        }
        private void ClearCartasUserControl(int ini, int fin)
        {
            var lst = (from Control uc in this.Controls
                       where uc is CartaUserControl && uc.TabIndex >= ini && uc.TabIndex <= fin
                       orderby uc.TabIndex ascending
                       select (CartaUserControl)uc).ToList();
            foreach (CartaUserControl uc in lst)
                uc.CartaPictureBox.Image = null;
        }


        private void DeseleccionarCartasUserControl(int ini, int fin)
        {
            var lst = (from Control uc in this.Controls
                       where uc is CartaUserControl && uc.TabIndex >= ini && uc.TabIndex <= fin
                       orderby uc.TabIndex ascending
                       select (CartaUserControl)uc).ToList();
            foreach (CartaUserControl uc in lst)
                uc.CartaPictureBox.BorderStyle = BorderStyle.None;
        }

        private void CargarCartas(List<Carta> origen, List<CartaUserControl> destino)
        {
   
            for (int i = 0; i < origen.Count; i++)

                destino[i].Carta = origen[i];

               



        }

        private void CargarCartasJugadorActivo()
        {
          
            ClearCartasUserControl(0, 6);
            CargarCartas(juego.JugadorActivo.Cartas, GetCartasUserControl(0, 6));
        }


        private void mostrarCartasVisibles()
        {
            if (!(juego.CartasVisible.Count == 0))
            {
                cartaUserControlMazo.Carta = juego.CartasVisible.First();
            }
            else
            {
                cartaUserControlMazo.Carta = null;
            }
        }

        private void mostrarCartasNoVisibles()
        {

            if (!(juego.CartasNoVisible.Count == 0))
            { 
            cartaUserControlMazoNoVisible.Carta = juego.CartasNoVisible.Last();
            cartaUserControlMazoNoVisible.CartaPictureBox.Image = Image.FromFile(@"img\cartas\atras.jpg");
            }
            else
            {
                juego.CartasNoVisible = juego.CartasVisible.ToList();
                juego.CartasVisible.Clear();
          
                cartaUserControlMazoNoVisible.Carta = juego.CartasNoVisible.Last();
                cartaUserControlMazoNoVisible.CartaPictureBox.Image = Image.FromFile(@"img\cartas\atras.jpg");
               


            }
        }

        private void MostrarJugadorActivo()
        {
            LblJugadorActivo.Text = juego.JugadorActivo.NombreCompleto;
        }

        private void btnTomarCarta_Click(object sender, EventArgs e)
        {
            if (turno.CartasSeleccionadas.Count == 0)

            {
                MessageBox.Show("Seleccione una carta");
                return;
            }

            // actualizar cartas jugador activo
            juego.JugadorActivo.Cartas.Add(juego.CartasVisible.First());
            juego.JugadorActivo.Cartas = juego.JugadorActivo.Cartas.Except(turno.CartasSeleccionadas).ToList();

            // actualizar cartas visibles
            juego.CartasVisible.RemoveAt(0);
            juego.CartasVisible.InsertRange(0, turno.CartasSeleccionadas);

            DeseleccionarCartasUserControl(0, 6);
            turno.CartasSeleccionadas.Clear();

            mostrarCartasVisibles();
            CargarCartasJugadorActivo();
            juego.JugadorActivo.TipoMovimiento = btnTomarCarta.Text.ToString();
            //PruebaXML
            GestorBitacora.LogXml(juego.JugadorActivo);
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (turno.CartasSeleccionadas.Count == 0)

            {
                MessageBox.Show("Seleccione una carta");
                return;
            }

            // actualizar cartas jugador activo
            juego.JugadorActivo.Cartas.Add(juego.CartasNoVisible.First());
            juego.JugadorActivo.Cartas = juego.JugadorActivo.Cartas.Except(turno.CartasSeleccionadas).ToList();

            // actualizar cartas no visibles
            juego.CartasNoVisible.RemoveAt(0);
            juego.CartasVisible.InsertRange(0, turno.CartasSeleccionadas);

            DeseleccionarCartasUserControl(0, 6);
            turno.CartasSeleccionadas.Clear();

            mostrarCartasNoVisibles();
            mostrarCartasVisibles();
            CargarCartasJugadorActivo();

            //PruebaXML
            Juego.JugadorActivo.TipoMovimiento = button5.Text.ToString();
            GestorBitacora.LogXml(juego.JugadorActivo);
        }

        private void Carta_Click(CartaUserControl ucCarta)
        {
            if (ucCarta.Carta != null)
            {
                DeseleccionarCartasUserControl(0, 6);
                ucCarta.CartaPictureBox.BorderStyle = BorderStyle.Fixed3D;
                turno.AgregarCarta(ucCarta.Carta);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            juego.CambiarTurnoJugador(0);
            ClearCartasUserControl(0, 6);
            CargarCartas(juego.JugadorActivo.Cartas, GetCartasUserControl(0, 6));
            MostrarJugadorActivo();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            juego.CambiarTurnoJugador(1);
            ClearCartasUserControl(0, 6);
            CargarCartas(juego.JugadorActivo.Cartas, GetCartasUserControl(0, 6));
            MostrarJugadorActivo();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            juego.CambiarTurnoJugador(2);
            ClearCartasUserControl(0, 6);
            CargarCartas(juego.JugadorActivo.Cartas, GetCartasUserControl(0, 6));
            MostrarJugadorActivo();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            juego.CambiarTurnoJugador(3);
            ClearCartasUserControl(0, 6);
            CargarCartas(juego.JugadorActivo.Cartas, GetCartasUserControl(0, 6));
            MostrarJugadorActivo();
        }

        private void EnlazarGrilaJugadores()
        {
            GrillaJugadores.DataSource = null;


            GrillaJugadores.DataSource = juego.Jugadores;

        }

        private void btnCortarMeza_Click(object sender, EventArgs e)
        {
           


            CortarMesaRespuesta cortarMesaRespuesta = GestorJuego.CortarMeza(juego.JugadorActivo.Cartas);


            if (cortarMesaRespuesta.PuedeCortarMesa)
            {
                int cantJugadores101 = 0;
                foreach (Jugador j in juego.Jugadores)
                {

                    CortarMesaRespuesta r = GestorJuego.CortarMeza(j.Cartas);
                    j.CartasSobrantes = r.CartasJugadorSobrantes;
                    // suma 
                    j.CartasSumadas += j.CartasSobrantes.Sum(x => x.Numero);

                    if (j.CartasSumadas >= 101)
                    {
                        cantJugadores101++;
                    }
                }

                // termino juego?
                if (cantJugadores101 > 0)
                {
                    int i = 0;
                    int min = 0;
                    Jugador jugadorGanador = null;
                    EnlazarGrilaJugadores();

                    foreach (Jugador j in juego.Jugadores)
                    {
                        if (i == 0 || j.CartasSumadas < min)
                        {
                            min = j.CartasSumadas;
                            jugadorGanador = j;
                        }
                        i++;
                    }

                    jugadorGanador.PartidasGanadas++;
                    jugadorGanador.PartidasJugadas++;
                    jugadorGanador.FechaHoraFinPartida = DateTime.Now;
                    TimeSpan pp = jugadorGanador.FechaHoraFinPartida.Subtract(jugadorGanador.FechaHoraInicioPartida);
                    int tiempojugadog = 0;
                    tiempojugadog = int.Parse(pp.Minutes.ToString());
                    jugadorGanador.TotalTiempoJugado = jugadorGanador.TotalTiempoJugado+ tiempojugadog;
                    //PruebaXML
                    GestorBitacora.LogXml(jugadorGanador);
                    jugadorGanador.TipoMovimiento = btnCortarMeza.Text.ToString();
                    GestorJugador.Edit(jugadorGanador);

                    List<Jugador> perdedores = juego.Jugadores.Where(x => x.Id != jugadorGanador.Id).ToList();
                    foreach (Jugador j in perdedores)
                    {
                        j.PartidasPerdidas++;
                        j.PartidasJugadas++;
                        j.FechaHoraFinPartida = DateTime.Now;
                        TimeSpan po = j.FechaHoraFinPartida.Subtract(j.FechaHoraInicioPartida);
                        int tiempojugado = 0;
                        tiempojugado = int.Parse(po.Minutes.ToString());
                        j.TotalTiempoJugado = j.TotalTiempoJugado + tiempojugado;

                        GestorJugador.Edit(j);
                    }
                    MessageBox.Show(string.Format("{0} GANO EL JUEGO", jugadorGanador.NombreCompleto));
                }
                else
                {
                    MessageBox.Show(string.Format("{0} GANO LA PARTIDA", juego.JugadorActivo.NombreCompleto));
                   
                }
                EnlazarGrilaJugadores();
              
            }
            else
            {
                MessageBox.Show("No puede cortar mesa");
            }

           

        }

        private void btnReiniciarPartida_Click(object sender, EventArgs e)
        {
           

            foreach (Jugador j in juego.Jugadores)
            {
             
                j.Cartas.Clear();

            }
            juego.CartasNoVisible.Clear();
            juego.CartasVisible.Clear();
            ClearCartasUserControl(0, 6);

            GestorJuego.ReiniciarJuego(juego);

            mostrarCartasVisibles();

            mostrarCartasNoVisibles();

            juego.JugadorActivo.Cartas = GestorJuego.TestDosEscalerasIgualPalo1();
        }
    }
}
