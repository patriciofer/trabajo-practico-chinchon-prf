﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DAL
{
    public class BitacoraMapper
    {
        private ConnectionManager connection = new ConnectionManager();

        public int Create(List<Bitacora> items)
        {
            int r = 0;

            connection.Open();
            connection.BeginTx();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@IdTipoBitacoraItem", 0));
            parameters.Add(connection.createParameter<string>("@IdJugador", 0));

            foreach (Bitacora b in items)
            {
                parameters[0].Value = b.TipoBitacoraItem.Id;
                parameters[1].Value = b.Jugador.Id;
                r = connection.Write("CreateBitacoraItem", parameters);
                if (r == -1)
                    break;
            }

            if (r == -1)
                connection.RollbackTx();
            else
                connection.CommitTx();
            connection.Close();
            return r;
        }

        public int Create(Bitacora bitacora)
        {
            int r;

            connection.Open();

            List<SqlParameter> parameteres = new List<SqlParameter>();
            parameteres.Add(connection.createParameter<int>("@IdTipoBitacoraItem", bitacora.TipoBitacoraItem.Id));
            parameteres.Add(connection.createParameter<int>("@IdJugador", bitacora.Jugador.Id));

            r = connection.Write("CreateBitacoraItem", parameteres);

            connection.Close();

            return r;
        }

        public int CreateXML (Jugador jugador)
        {
            DataSet ds = new DataSet();
            string esquema = "C:\\BitacoraXML\\BitacoraMovimiento.xml";
             
            ds.ReadXmlSchema(esquema);
            ds.ReadXml(esquema);

            DataRow registro = ds.Tables[0].NewRow();
            registro[0] = jugador.Id;
            registro[1] = jugador.TipoMovimiento;
            registro[2] = DateTime.Now;

            ds.Tables[0].Rows.Add(registro);
            ds.WriteXml(esquema);
           
            return 1;
        }


    }
}
