﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
    public class JugadorMapper  
    {
        private ConnectionManager connection = new ConnectionManager();

        public int Create(Jugador jugador)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@nombre", jugador.Nombre));
            parameters.Add(connection.createParameter<string>("@apellido", jugador.Apellido));
            parameters.Add(connection.createParameter<string>("@usuario", jugador.Usuario));
            parameters.Add(connection.createParameter<string>("@clave", jugador.Clave));
            parameters.Add(connection.createParameter<int>("@PartidasGanadas", jugador.PartidasGanadas));
            parameters.Add(connection.createParameter<int>("@PartidasPerdidas", jugador.PartidasPerdidas));
            parameters.Add(connection.createParameter<int>("@PartidasEmpatadas", jugador.PartidasEmpatadas));
            parameters.Add(connection.createParameter<int>("@TotalTiempoJugado", jugador.TotalTiempoJugado));
            parameters.Add(connection.createParameter<int>("@PartidasJugadas", jugador.PartidasJugadas));

            connection.Open();
            int r = connection.Write("CreateUsuario", parameters);
            connection.Close();

            return r;
        }

        public int Edit(Jugador jugador)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@id", jugador.Id));
            parameters.Add(connection.createParameter<string>("@PartidasGanadas", jugador.PartidasGanadas));
            parameters.Add(connection.createParameter<string>("@PartidasPerdidas", jugador.PartidasPerdidas));
            parameters.Add(connection.createParameter<string>("@PartidasEmpatadas", jugador.PartidasEmpatadas));
            parameters.Add(connection.createParameter<int>("@TotalTiempoJugado", jugador.TotalTiempoJugado));
            parameters.Add(connection.createParameter<int>("@PartidasJugadas", jugador.PartidasJugadas));

            connection.Open();
            int r = connection.Write("EditUsuario", parameters);
            connection.Close();

            return r;
        }

        public int Remove(Jugador jugador)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@id", jugador.Id));
            connection.Open();
            int r = connection.Write("RemoveJugador", parameters);
            connection.Close();
            return r;
        }

        public List<Jugador> GetAll()
        {
            List<Jugador> lst = new List<Jugador>();
            connection.Open();
            DataTable table = connection.Read("GetUsers", null);
            connection.Close();

            foreach (DataRow row in table.Rows)
            {
                Jugador j = new Jugador();
                j.Id = Int32.Parse(row["Id"].ToString());
                j.Nombre = row["Nombre"].ToString();
                j.Apellido = row["Apellido"].ToString();
                j.Usuario = row["Usuario"].ToString();
                j.Clave = row["Clave"].ToString();
                j.PartidasGanadas = int.Parse(row["PartidasGanadas"].ToString());
                j.PartidasPerdidas = int.Parse(row["PartidasPerdidas"].ToString());
                j.PartidasEmpatadas = int.Parse(row["PartidasEmpatadas"].ToString());
                j.TotalTiempoJugado = int.Parse(row["TotalTiempoJugado"].ToString());
                j.PartidasJugadas = int.Parse(row["PartidasJugadas"].ToString());
                lst.Add(j);
            }

            return lst;
        }

        public int CheckPlayerByCredentials(Jugador jugador)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@usuario", jugador.Usuario));
            connection.Open();
            int r = connection.ReadScalar("CheckPlayerByCredentials", parameters);
            connection.Close();
            return r;
        }

        public Jugador FindPlayerByCredentials(Jugador jugador)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@usuario", jugador.Usuario));
            parameters.Add(connection.createParameter<string>("@clave", jugador.Clave));

            connection.Open();
            DataTable table = connection.Read("FindPlayerByCredentials", parameters);
            connection.Close();

            Jugador j = new Jugador();
            foreach (DataRow row in table.Rows)
            {
                j.Id = Int32.Parse(row["Id"].ToString());
                j.Nombre = row["Nombre"].ToString();
                j.Apellido = row["Apellido"].ToString();
                j.Usuario = row["Usuario"].ToString();
                j.Clave = row["Clave"].ToString();
                j.PartidasGanadas = int.Parse(row["PartidasGanadas"].ToString());
                j.PartidasPerdidas = int.Parse(row["PartidasPerdidas"].ToString());
                j.PartidasEmpatadas = int.Parse(row["PartidasEmpatadas"].ToString());
                j.TotalTiempoJugado = int.Parse(row["TotalTiempoJugado"].ToString());
                j.PartidasJugadas = int.Parse(row["PartidasJugadas"].ToString());
            }
            return j;
        }

        public int FindPlayerByUser(Jugador jugador)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(connection.createParameter<string>("@usuario", jugador.Usuario));
            connection.Open();
            int r = connection.ReadScalar("FindPlayerByUser", parameters);
            connection.Close();
            return r;
        }
    }
}
