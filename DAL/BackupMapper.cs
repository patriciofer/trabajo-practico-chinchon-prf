﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace DAL
{
    public class BackupMapper

    {
        public static void RealizarBK(string ubicacion)
        {
            string nombreBd = ConfigurationManager.AppSettings.Get("nombreBdPrincipal");
            string query = "BACKUP DATABASE " + nombreBd + " TO DISK = @ubicacion";
            SqlParameter[] parameters =
            {
                new SqlParameter("@ubicacion", ubicacion)
            };
           ConnectionManager.Ejecutar(query, parameters);
        }

        public static void RealizarRestore(string ubicacion)
        {
            string nombreBd = ConfigurationManager.AppSettings.Get("nombreBdPrincipal");
            string query = "ALTER DATABASE " + nombreBd + " SET SINGLE_USER WITH ROLLBACK IMMEDIATE; " +
                "RESTORE DATABASE " + nombreBd + " FROM DISK = @ubicacion WITH REPLACE; " +
                "ALTER DATABASE " + nombreBd + " SET MULTI_USER;"; ;
            SqlParameter[] parameters =
            {
                new SqlParameter("@ubicacion", ubicacion)
            };
            ConnectionManager.BuscarEnMaster(query, parameters);
        }
    }
}
