﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
    internal class ConnectionManager
    {
        private SqlConnection connection;
        private SqlTransaction tx;

        public void Open()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStrDb"].ConnectionString);
            connection.Open();
        }

        public void Close()
        {
            if (connection != null && connection.State == ConnectionState.Open)
            {
                connection.Close();
                connection = null;
            }
        }

        public void BeginTx()
        {
            if (tx == null)
                tx = connection.BeginTransaction();
        }

        public void CommitTx()
        {
            tx.Commit();
            tx = null;
        }

        public void RollbackTx()
        {
            tx.Rollback();
            tx = null;
        }

        public int Write(string commandName, List<SqlParameter> parameters)
        {
            int rowsAffected = 0;

            using (SqlCommand cmd = CreateCommand(commandName, parameters))
            {
                try
                {
                    rowsAffected = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    rowsAffected = -1;
                }
                cmd.Parameters.Clear();
            }
            return rowsAffected;
        }

        public DataTable Read(string commandName, List<SqlParameter> parameters)
        {
            DataTable table = new DataTable();

            using (SqlDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = CreateCommand(commandName, parameters);
                try
                {
                    adapter.Fill(table);
                }
                catch (Exception ex)
                {
                    table = null;
                }
            }
            return table;
        }

        public int ReadScalar(string commandName, List<SqlParameter> parameters)
        {
            int rowsAffected = 0;
            using (SqlCommand cmd = CreateCommand(commandName, parameters))
            {
                try
                {
                    rowsAffected = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    rowsAffected = -1;
                }
            }
            return rowsAffected;
        }

        public SqlParameter createParameter<T>(string name, object value)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = name;
            parameter.DbType = SqlParameterHelper.LookupDbType(typeof(T), name);
            parameter.Value = value;
            return parameter;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }

        public SqlParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Single;
            return p;
        }

        public SqlCommand CreateCommand(string spName, List<SqlParameter> parameters)
        {
            SqlCommand cmd = new SqlCommand(spName, connection);

            if (parameters != null && parameters.Count > 0)
                cmd.Parameters.AddRange(parameters.ToArray());

            if (tx != null)
                cmd.Transaction = tx;

            cmd.CommandType = CommandType.StoredProcedure;

            return cmd;
        }

        private const string CONN_STRING_PRINCIPAL_KEY = "ConnStrDb";
        private const string CONN_STRING_MASTER_KEY = "Master";

        public static void BuscarEnMaster(string query, SqlParameter[] parameters)
        {
            Ejecutar(query, parameters, CONN_STRING_MASTER_KEY);
        }

        public static void Ejecutar(string query, SqlParameter[] parameters)
        {
            Ejecutar(query, parameters, CONN_STRING_PRINCIPAL_KEY);
        }


        private static void Ejecutar(string query, SqlParameter[] parameters, string connStringKey)
        {
            string connString = ConfigurationManager.ConnectionStrings[connStringKey].ConnectionString;
             
                using (SqlConnection connection = new SqlConnection(connString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddRange(parameters);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
             
            
        }
    }
}
