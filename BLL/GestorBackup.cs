﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;


namespace BLL
{
    public class GestorBackup

    {
        private const string DATE_FORMAT_FOR_FILE = "yyyy-MM-dd-HH-mm-ss";

        public static bool RealizarBK( )
        {
           
                    string rutaBackup = ConfigurationManager.AppSettings.Get("ubicacionBackups");
                    Directory.CreateDirectory(rutaBackup);

                    string nombreArchivo = string.Format("Chinchon_{0}", DateTime.Now.ToString(DATE_FORMAT_FOR_FILE));
                    BackupMapper.RealizarBK(rutaBackup + nombreArchivo + ".bak");

                  return true;
                
          

            
        }

        public static List<Backup> GetBkps()
        {
            
                string rutaBackups = ConfigurationManager.AppSettings.Get("ubicacionBackups");
                string[] archivos = Directory.GetFiles(rutaBackups, "*.bak");

                List<Backup> backups = new List<Backup>();
                for (int i = 0; i < archivos.Length; i++)
                {
                    string nombreArchivo = Path.GetFileNameWithoutExtension(archivos[i]);
                    string fechabkp = nombreArchivo.Split('_')[1];
                    backups.Add(new Backup
                    {
                        Fecha = fechabkp,
                        NombreArchivo = nombreArchivo
                    });
                }

                return backups;
            
             
        }

        public static bool RealizarRestore(string nombreArchivo)
        {
           
                string rutaBackups = ConfigurationManager.AppSettings.Get("ubicacionBackups");
                string rutaCompletaBackup = rutaBackups + nombreArchivo + ".bak";
                if (File.Exists(rutaCompletaBackup))
                {
                    BackupMapper.RealizarRestore(rutaCompletaBackup);
                    return true;
                }
             
            
            return false;
        }
    }
}
