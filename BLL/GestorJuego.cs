﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class GestorJuego
    {

        public void IniciarJuego(Juego juego)
        {

       

            juego.Cartas = IniciarMazoDeCartas(juego.Cartas);

            RepartirCartas(juego.Cartas, juego.Jugadores);

            IniciarMazoVisible(juego.Cartas, juego.CartasVisible);

            IniciarMazoNoVisible(juego.Cartas, juego.CartasNoVisible);
 
        }
      
        public void ReiniciarJuego(Juego juego)
        {
        
            juego.Cartas = IniciarMazoDeCartas(juego.Cartas);

            RepartirCartas(juego.Cartas, juego.Jugadores);

            IniciarMazoVisible(juego.Cartas, juego.CartasVisible);

            IniciarMazoNoVisible(juego.Cartas, juego.CartasNoVisible);
 
        }




        public void IniciarMazoVisible(List<Carta> cartas, List<Carta> cartasVisible)
        {
            cartasVisible.Add(cartas.First());
            cartas.RemoveAt(0);
        }

        public void IniciarMazoNoVisible(List<Carta> cartas, List<Carta> cartasNoVisible)
        {
            cartasNoVisible.AddRange(cartas);
        }

        public void RemoverCartaMazoVisible(List<Carta> cartasVisibles, List<Carta> cartasjugador)
        {
            cartasjugador.Add(cartasVisibles.First());
            cartasVisibles.RemoveAt(0);
        }

        public void DevolverCartaMazoVisible(List<Carta> cartasVisibles, List<Carta> cartasjugador, Carta carta)
        {
            cartasVisibles.Insert(0, carta);
            cartasjugador.Remove(carta);
        }

        public int getCantidadEscaleras(List<Carta> cartas, List<Carta> cartasEscalera)
        {
            if(cartas == null || cartas.Count == 0)
                return 0;

            List<string> palos = cartas.Select(x => x.Palo).Distinct().ToList();

            int cantEscaleras = 0;
            foreach(string p in palos)
            {
                List<Carta> cartasAux = cartas.Where(x => x.Palo == p).OrderBy(x => x.Numero).ToList();
                List<Carta> cartasEscaleraAux = new List<Carta>();
                for(int i = 0; i < cartasAux.Count - 1; i++)
                {
                    Carta cartaActual = cartasAux[i];
                    Carta cartaSiguiente = cartasAux[i + 1];
                    if((cartaActual.Numero + 1) == cartaSiguiente.Numero)
                    {
                        if(!cartasEscaleraAux.Any(x => x.Numero == cartaActual.Numero && x.Palo == cartaActual.Palo))
                            cartasEscaleraAux.Add(cartaActual);
                        cartasEscaleraAux.Add(cartaSiguiente);
                    }

                    if(((cartaActual.Numero + 1) != cartaSiguiente.Numero || (i + 1) == cartasAux.Count - 1) && cartasEscaleraAux.Count >= 3)
                    {
                        cantEscaleras++;
                        cartasEscalera.AddRange(cartasEscaleraAux);
                        cartasEscaleraAux.Clear();
                    }

                    if((cartaActual.Numero + 1) != cartaSiguiente.Numero)
                        cartasEscaleraAux.Clear();
                }
            }

            return cantEscaleras;
        }

        public int getCantidadPiernas(List<Carta> cartas, List<Carta> cartasPiernas)
        {

            List<int> Numeros = cartas.Select(x => x.Numero).Distinct().ToList();

            int cantPiernas = 0;

            foreach(int p in Numeros)
            {
                List<Carta> cartasAux = cartas.Where(x => x.Numero == p).OrderBy(x => x.Numero).ToList();
                List<Carta> cartasPiernaAux = new List<Carta>();
                for(int i = 0; i < cartasAux.Count - 1; i++)
                {
                    Carta cartaActual = cartasAux[i];
                    Carta cartaSiguiente = cartasAux[i + 1];
                    if((cartaActual.Numero) == cartaSiguiente.Numero)
                    {
                        if(!cartasPiernaAux.Any(x => x.Numero == cartaActual.Numero))
                            cartasPiernaAux.Add(cartaActual);
                        cartasPiernaAux.Add(cartaSiguiente);
                    }

                    if(((cartaActual.Numero) != cartaSiguiente.Numero || (i + 1) == cartasAux.Count - 1) && cartasPiernaAux.Count >= 3)
                    {
                        cantPiernas++;
                        cartasPiernas.AddRange(cartasPiernaAux);
                        cartasPiernaAux.Clear();
                    }

                    if((cartaActual.Numero) != cartaSiguiente.Numero)
                        cartasPiernaAux.Clear();
                }

            }

            return cantPiernas;

        }

        public CortarMesaRespuesta VerificarCortarMeza(List<Carta> cartasJugador)
        {
            CortarMesaRespuesta cortarMesaRespuesta = new CortarMesaRespuesta();
            cortarMesaRespuesta.CartasJugadorSobrantes = new List<Carta>();
            cortarMesaRespuesta.PuedeCortarMesa = false;
            List<Carta> cartasEscalera = new List<Carta>();
            List<Carta> cartasPiernas = new List<Carta>();
            // List<Carta> cartasJugador = TestDosEscalerasIgualPalo1();

            int cantEscaleras = getCantidadEscaleras(cartasJugador, cartasEscalera);
            cortarMesaRespuesta.CartasJugadorSobrantes = cartasJugador.Except(cartasEscalera).ToList();

            // chinchon
            if (cantEscaleras == 1 && cartasEscalera.Count == 7)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }
            // escalera de 6 cartas
            else if (cantEscaleras == 1 && cartasEscalera.Count == 6 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 1 && cortarMesaRespuesta.CartasJugadorSobrantes[0].Numero < 6)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }
            // 2 escaleras de 3
            else if (cantEscaleras == 2 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 1 && cortarMesaRespuesta.CartasJugadorSobrantes[0].Numero < 6)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }
            // 1 escalera de 3 y 1 escalera de 4
            else if (cantEscaleras == 2 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 0)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }

            int cantPiernas = getCantidadPiernas(cortarMesaRespuesta.CartasJugadorSobrantes, cartasPiernas);
            cortarMesaRespuesta.CartasJugadorSobrantes = cortarMesaRespuesta.CartasJugadorSobrantes.Except(cartasPiernas).ToList();

            // 2 piernas y 1 sobrante
            if (cantPiernas == 2 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 1 && cortarMesaRespuesta.CartasJugadorSobrantes[0].Numero < 6)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }
            // 2 piernas
            else if (cantPiernas == 2 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 0)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }
            // 1 escalera y 1 pierna y 1 carta sobrante
            else if (cantEscaleras == 1 && cantPiernas == 1 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 1 && cortarMesaRespuesta.CartasJugadorSobrantes[0].Numero < 6)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }
            // 1 escalera y 1 pierna y sin cartas sobrantes
            else if (cantPiernas == 1 && cantEscaleras == 1 && cortarMesaRespuesta.CartasJugadorSobrantes.Count == 0)
            {
                cortarMesaRespuesta.PuedeCortarMesa = true;
            }

            return cortarMesaRespuesta;
            
        }

        public CortarMesaRespuesta CortarMeza(List<Carta> cartasJugador)
        {
            return VerificarCortarMeza(cartasJugador);
        }

        public List<Carta> Test1()
        {

            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 1;
            cartas.Add(carta1);


            Carta carta2 = new Carta();
            carta2.Palo = "basto";
            carta2.Numero = 3;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "basto";
            carta3.Numero = 5;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "basto";
            carta4.Numero = 6;
            cartas.Add(carta4);


            Carta carta5 = new Carta();
            carta5.Palo = "basto";
            carta5.Numero = 8;
            cartas.Add(carta5);


            Carta carta6 = new Carta();
            carta6.Palo = "basto";
            carta6.Numero = 9;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "basto";
            carta7.Numero = 12;
            cartas.Add(carta7);

            return cartas;
        }

        public List<Carta> Test2()
        {

            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 2;
            cartas.Add(carta1);


            Carta carta2 = new Carta();
            carta2.Palo = "basto";
            carta2.Numero = 3;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "basto";
            carta3.Numero = 4;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "basto";
            carta4.Numero = 5;
            cartas.Add(carta4);


            Carta carta5 = new Carta();
            carta5.Palo = "basto";
            carta5.Numero = 6;
            cartas.Add(carta5);


            Carta carta6 = new Carta();
            carta6.Palo = "espada";
            carta6.Numero = 8;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "espada";
            carta7.Numero = 9;
            cartas.Add(carta7);

            return cartas;
        }

        public List<Carta> Test3()
        {
            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 1;
            cartas.Add(carta1);

            Carta carta2 = new Carta();
            carta2.Palo = "copa";
            carta2.Numero = 1;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "oro";
            carta3.Numero = 1;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "espada";
            carta4.Numero = 1;
            cartas.Add(carta4);

            Carta carta5 = new Carta();
            carta5.Palo = "copa";
            carta5.Numero = 6;
            cartas.Add(carta5);


            Carta carta6 = new Carta();
            carta6.Palo = "copa";
            carta6.Numero = 7;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "copa";
            carta7.Numero = 8;
            cartas.Add(carta7);

            return cartas;
        }

        public List<Carta> Test4()
        {

            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 1;
            cartas.Add(carta1);


            Carta carta2 = new Carta();
            carta2.Palo = "oro";
            carta2.Numero = 1;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "copa";
            carta3.Numero = 1;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "espada";
            carta4.Numero = 1;
            cartas.Add(carta4);


            Carta carta5 = new Carta();
            carta5.Palo = "basto";
            carta5.Numero = 8;
            cartas.Add(carta5);


            Carta carta6 = new Carta();
            carta6.Palo = "oro";
            carta6.Numero = 8;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "copa";
            carta7.Numero = 5;
            cartas.Add(carta7);

            return cartas;
        }

        public List<Carta> TestDosEscalerasIgualPalo1()
        {
            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 1;
            cartas.Add(carta1);

            Carta carta2 = new Carta();
            carta2.Palo = "basto";
            carta2.Numero = 2;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "basto";
            carta3.Numero = 3;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "copa";
            carta4.Numero = 4;
            cartas.Add(carta4);

            Carta carta5 = new Carta();
            carta5.Palo = "basto";
            carta5.Numero = 6;
            cartas.Add(carta5);

            Carta carta6 = new Carta();
            carta6.Palo = "basto";
            carta6.Numero = 7;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "basto";
            carta7.Numero = 8;
            cartas.Add(carta7);

            return cartas;
        }

        public List<Carta> TestDosEscalerasIgualPalo2()
        {
            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 1;
            cartas.Add(carta1);

            Carta carta2 = new Carta();
            carta2.Palo = "basto";
            carta2.Numero = 2;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "basto";
            carta3.Numero = 3;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "basto";
            carta4.Numero = 5;
            cartas.Add(carta4);

            Carta carta5 = new Carta();
            carta5.Palo = "basto";
            carta5.Numero = 6;
            cartas.Add(carta5);

            Carta carta6 = new Carta();
            carta6.Palo = "basto";
            carta6.Numero = 7;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "basto";
            carta7.Numero = 8;
            cartas.Add(carta7);

            return cartas;
        }

        public List<Carta> TestDosEscalerasIgualPalo3()
        {
            List<Carta> cartas = new List<Carta>();

            Carta carta1 = new Carta();
            carta1.Palo = "basto";
            carta1.Numero = 1;
            cartas.Add(carta1);

            Carta carta2 = new Carta();
            carta2.Palo = "basto";
            carta2.Numero = 2;
            cartas.Add(carta2);

            Carta carta3 = new Carta();
            carta3.Palo = "basto";
            carta3.Numero = 3;
            cartas.Add(carta3);

            Carta carta4 = new Carta();
            carta4.Palo = "basto";
            carta4.Numero = 5;
            cartas.Add(carta4);

            Carta carta5 = new Carta();
            carta5.Palo = "basto";
            carta5.Numero = 6;
            cartas.Add(carta5);

            Carta carta6 = new Carta();
            carta6.Palo = "basto";
            carta6.Numero = 7;
            cartas.Add(carta6);

            Carta carta7 = new Carta();
            carta7.Palo = "espada";
            carta7.Numero = 1;
            cartas.Add(carta7);

            return cartas;
        }


     

        public void RepartirCartas(List<Carta> cartas, List<Jugador> jugadores)
        {
            foreach (Jugador j in jugadores)
            {
                for (int i = 0; i < 7; i++)
                {
                    j.Cartas.Add(cartas.First());
                    cartas.RemoveAt(0);
                }
            }
        }

        public string ObtenerPalo(int i)
        {
            string palo = "";

            switch (i)
            {
                case 1:
                    palo = "basto";

                    break;
                case 2:
                    palo = "copa";

                    break;

                case 3:
                    palo = "oro";

                    break;
                case 4:
                    palo = "espada";

                    break;
            }

            return palo;
        }

        public List<Carta> IniciarMazoDeCartas(List<Carta> cartas)
        {
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= 12; j++)
                {
                    Carta c = new Carta();
                    c.Numero = j;
                    c.Palo = ObtenerPalo(i);
                    cartas.Add(c);
                }
            }
          

            return MezclarMazo(cartas);
        }

        public List<Carta> MezclarMazo(List<Carta> cartas)
        {
            return RandomizedList(cartas).ToList();
        }

        ////Invocar al metodo mezclarCartas
        private IList<T> RandomizedList<T>(IEnumerable<T> source)
        {
            Random r = new Random();
            var list = new List<T>();

            foreach (var item in source)
            {
                var i = r.Next(list.Count + 1);
                if (i == list.Count)
                    list.Add(item);
                else
                {
                    var temp = list[i];
                    list[i] = item;
                    list.Add(temp);
                }
            }
            return list;
        }

    }
}
