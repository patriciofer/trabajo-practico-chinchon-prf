﻿using BE;
using DAL;
using System;
using System.Collections.Generic;

namespace BLL
{
    public class GestorBitacora
    {
        private BitacoraMapper bitacoraMapper = new BitacoraMapper();

        public int Create(List<Bitacora> items)
        {
            return bitacoraMapper.Create(items);
        }

        public int Create(Bitacora bitacora)
        {
            return bitacoraMapper.Create(bitacora);
        }

        public Bitacora GetNewBitacora(Jugador jugador, TipoBitacoraItemEnum tipo)
        {
            return new Bitacora() { Jugador = jugador, TipoBitacoraItem = new TipoBitacoraItem() { Id = (int)tipo } };
        }

        public void LogComenzarPartida(List<Jugador> jugadores)
        {
            List<Bitacora> items = new List<Bitacora>();
            foreach (Jugador j in jugadores)
                items.Add(GetNewBitacora(j, TipoBitacoraItemEnum.ComienzoDePartida));
            Create(items);
        }

        public void LogIniciarSesion(Jugador jugador)
        {
            Create(GetNewBitacora(jugador, TipoBitacoraItemEnum.InicioDeSesion));
        }

        public void LogCerrarSesion(Jugador jugador)
        {
            Create(GetNewBitacora(jugador, TipoBitacoraItemEnum.CierreDeSesion));
        }

        public void LogXml(Jugador jugador)
        {
           bitacoraMapper.CreateXML(jugador);
        }



        public void LogFinalizarPartida(Jugador jugador)
        {
            Create(GetNewBitacora(jugador, TipoBitacoraItemEnum.FinalizaciónDePartida));
        }

        public void LogFinalizarPartida(List<Jugador> jugadores)
        {
            List<Bitacora> items = new List<Bitacora>();
            foreach (Jugador j in jugadores)
                items.Add(GetNewBitacora(j, TipoBitacoraItemEnum.FinalizaciónDePartida));
            Create(items);
        }

    }
}
