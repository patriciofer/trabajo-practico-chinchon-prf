﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;
 

namespace BLL
{
    public class GestorJugador

    {
        private JugadorMapper jugadorMapper = new JugadorMapper();

        public int Create(Jugador jugador)
        {
            return jugadorMapper.Create(jugador);
        }

        public int Edit(Jugador jugador)
        {
            return jugadorMapper.Edit(jugador);
        }

        public int Remove(Jugador jugador)
        {
            return jugadorMapper.Remove(jugador);
        }

        public List<Jugador> GetAll()
        {
            return jugadorMapper.GetAll();
        }

        public int CheckPlayerByCredentials(Jugador jugador)
        {
            return jugadorMapper.CheckPlayerByCredentials(jugador);
        }

        public Jugador FindPlayerByCredentials(Jugador jugador)
        {
            return jugadorMapper.FindPlayerByCredentials(jugador);
        }

        public int FindPlayerByUser(Jugador jugador)
        {
            return jugadorMapper.FindPlayerByUser(jugador);
        }
    }
}

