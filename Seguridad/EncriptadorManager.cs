﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;
namespace Seguridad
{
    public class EncriptadorManager
    {
        private static HashAlgorithm hashAlgorithm;

        private static string createString(byte[] value)
        {
            StringBuilder sb = new StringBuilder();
            for (var index = 0; index <= value.Length - 1; index++)
                sb.Append(value[index].ToString("x2"));
            return sb.ToString();
        }

        public static string DesencriptadoSimetrico(string textoCifrado)
        {
            string EncryptionKey = ConfigurationManager.AppSettings["CryptoKey"]; // PARAMETRIZACIÓN
            byte[] cipherBytes = Convert.FromBase64String(textoCifrado);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    textoCifrado = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return textoCifrado;
        }

        public static string EncriptadoSimetrico(string textoPlano)
        {
            string EncryptionKey = ConfigurationManager.AppSettings["CryptoKey"]; // PARAMETRIZACIÓN
            byte[] clearBytes = Encoding.Unicode.GetBytes(textoPlano);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    textoPlano = Convert.ToBase64String(ms.ToArray());
                }
            }
            return textoPlano;
        }

        public static string Hash(string value)
        {
            hashAlgorithm = MD5.Create();
            byte[] data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(value));
            return createString(data);
        }

        public static bool Compare(string input, string hashedInput)
        {
            var value = Hash(input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            return comparer.Compare(value, hashedInput).Equals(0);
        }
    }
}