USE [Chinchon]
GO
/****** Object:  Table [dbo].[Bitacora]    Script Date: 17/11/2020 10:53:57 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bitacora](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoBitacoraItem] [int] NOT NULL,
	[FechaHora] [datetime] NOT NULL,
	[IdJugador] [int] NOT NULL,
 CONSTRAINT [PK_Bitacora] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jugador]    Script Date: 17/11/2020 10:53:57 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jugador](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Clave] [varchar](50) NOT NULL,
	[PartidasGanadas] [int] NOT NULL,
	[PartidasPerdidas] [int] NOT NULL,
	[PartidasEmpatadas] [int] NOT NULL,
	[TotalTiempoJugado] [int] NOT NULL,
	[PartidasJugadas] [int] NOT NULL,
 CONSTRAINT [PK_Jugador] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoBitacoraItem]    Script Date: 17/11/2020 10:53:57 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoBitacoraItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TipoBitacoraItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bitacora] ON 

INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (1, 1, CAST(N'2020-11-10T12:26:10.823' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (2, 1, CAST(N'2020-11-10T12:26:23.247' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (3, 1, CAST(N'2020-11-10T12:26:58.440' AS DateTime), 4)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (4, 1, CAST(N'2020-11-10T12:27:10.797' AS DateTime), 5)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (5, 3, CAST(N'2020-11-10T12:27:12.523' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (6, 3, CAST(N'2020-11-10T12:27:12.530' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (7, 3, CAST(N'2020-11-10T12:27:12.537' AS DateTime), 4)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (8, 3, CAST(N'2020-11-10T12:27:12.543' AS DateTime), 5)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (11, 1, CAST(N'2020-11-17T09:36:19.277' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (12, 1, CAST(N'2020-11-17T09:36:31.360' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (13, 1, CAST(N'2020-11-17T09:36:43.030' AS DateTime), 7)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (14, 1, CAST(N'2020-11-17T09:36:54.733' AS DateTime), 5)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (15, 3, CAST(N'2020-11-17T09:36:55.760' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (16, 3, CAST(N'2020-11-17T09:36:55.773' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (17, 3, CAST(N'2020-11-17T09:36:55.787' AS DateTime), 7)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (18, 3, CAST(N'2020-11-17T09:36:55.800' AS DateTime), 5)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (19, 1, CAST(N'2020-11-17T10:00:02.763' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (20, 1, CAST(N'2020-11-17T10:00:10.470' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (21, 1, CAST(N'2020-11-17T10:00:20.197' AS DateTime), 3)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (22, 1, CAST(N'2020-11-17T10:00:35.003' AS DateTime), 7)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (23, 3, CAST(N'2020-11-17T10:00:36.230' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (24, 3, CAST(N'2020-11-17T10:00:36.243' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (25, 3, CAST(N'2020-11-17T10:00:36.257' AS DateTime), 3)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (26, 3, CAST(N'2020-11-17T10:00:36.270' AS DateTime), 7)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (27, 1, CAST(N'2020-11-17T10:23:29.293' AS DateTime), 8)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (28, 1, CAST(N'2020-11-17T10:23:37.533' AS DateTime), 6)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (29, 1, CAST(N'2020-11-17T10:24:23.683' AS DateTime), 7)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (30, 1, CAST(N'2020-11-17T10:24:34.580' AS DateTime), 3)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (31, 3, CAST(N'2020-11-17T10:24:35.573' AS DateTime), 8)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (32, 3, CAST(N'2020-11-17T10:24:35.580' AS DateTime), 6)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (33, 3, CAST(N'2020-11-17T10:24:35.590' AS DateTime), 7)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (34, 3, CAST(N'2020-11-17T10:24:35.600' AS DateTime), 3)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (36, 1, CAST(N'2020-11-17T10:29:30.387' AS DateTime), 8)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (37, 1, CAST(N'2020-11-17T10:29:39.867' AS DateTime), 6)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (40, 1, CAST(N'2020-11-17T10:31:35.583' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (42, 1, CAST(N'2020-11-17T10:32:22.513' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (43, 1, CAST(N'2020-11-17T10:32:29.760' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (44, 1, CAST(N'2020-11-17T10:32:37.810' AS DateTime), 4)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (45, 1, CAST(N'2020-11-17T10:32:48.927' AS DateTime), 5)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (46, 3, CAST(N'2020-11-17T10:32:49.830' AS DateTime), 1)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (47, 3, CAST(N'2020-11-17T10:32:49.837' AS DateTime), 2)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (48, 3, CAST(N'2020-11-17T10:32:49.847' AS DateTime), 4)
INSERT [dbo].[Bitacora] ([Id], [IdTipoBitacoraItem], [FechaHora], [IdJugador]) VALUES (49, 3, CAST(N'2020-11-17T10:32:49.853' AS DateTime), 5)
SET IDENTITY_INSERT [dbo].[Bitacora] OFF
GO
SET IDENTITY_INSERT [dbo].[Jugador] ON 

INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (1, N'Pepe', N'Argento', N'pepe', N'123', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (2, N'Tito', N'Argento', N'tito', N'123', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (3, N'Ben', N'Stiller', N'ben', N'123', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (4, N'Robert', N'De Niro', N'robert', N'123', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (5, N'pato', N'fer', N'pato', N'234', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (6, N'jorge', N'gol', N'jor', N'234', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (7, N'pantano', N'nuevo', N'panta', N'234', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (8, N'Kakaroto', N'Name', N'goku', N'234', 0, 0, 0, 0, 0)
INSERT [dbo].[Jugador] ([Id], [Nombre], [Apellido], [Usuario], [Clave], [PartidasGanadas], [PartidasPerdidas], [PartidasEmpatadas], [TotalTiempoJugado], [PartidasJugadas]) VALUES (9, N'Patricio', N'Fernandez', N'prf', N'456', 0, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Jugador] OFF
GO
SET IDENTITY_INSERT [dbo].[TipoBitacoraItem] ON 

INSERT [dbo].[TipoBitacoraItem] ([Id], [Nombre]) VALUES (1, N'Inicio de sesión')
INSERT [dbo].[TipoBitacoraItem] ([Id], [Nombre]) VALUES (2, N'Cierre de sesión')
INSERT [dbo].[TipoBitacoraItem] ([Id], [Nombre]) VALUES (3, N'Comienzo de partida')
INSERT [dbo].[TipoBitacoraItem] ([Id], [Nombre]) VALUES (4, N'Finalización de partida')
SET IDENTITY_INSERT [dbo].[TipoBitacoraItem] OFF
GO
ALTER TABLE [dbo].[Bitacora]  WITH CHECK ADD  CONSTRAINT [FK_Bitacora_Jugador] FOREIGN KEY([IdJugador])
REFERENCES [dbo].[Jugador] ([Id])
GO
ALTER TABLE [dbo].[Bitacora] CHECK CONSTRAINT [FK_Bitacora_Jugador]
GO
ALTER TABLE [dbo].[Bitacora]  WITH CHECK ADD  CONSTRAINT [FK_Bitacora_TipoBitacoraItem] FOREIGN KEY([IdTipoBitacoraItem])
REFERENCES [dbo].[TipoBitacoraItem] ([Id])
GO
ALTER TABLE [dbo].[Bitacora] CHECK CONSTRAINT [FK_Bitacora_TipoBitacoraItem]
GO
/****** Object:  StoredProcedure [dbo].[CheckPlayerByCredentials]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CheckPlayerByCredentials]
	@usuario AS VARCHAR(50)
AS
BEGIN
	SELECT COUNT(*) AS CANTIDAD FROM Jugador WHERE Usuario=@usuario
END

GO
/****** Object:  StoredProcedure [dbo].[CreateBitacoraItem]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateBitacoraItem]
@IdTipoBitacoraItem AS INT,
@IdJugador AS INT
AS
BEGIN
	INSERT INTO Bitacora
	(IdTipoBitacoraItem,FechaHora,IdJugador) VALUES
	(@IdTipoBitacoraItem,GETDATE(),@IdJugador)
END


GO
/****** Object:  StoredProcedure [dbo].[CreateUsuario]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateUsuario]
	@nombre AS VARCHAR(50),
	@apellido AS VARCHAR(50),
	@usuario AS VARCHAR(50),
	@clave AS VARCHAR(50),
	@partidasGanadas AS INT,
	@partidasPerdidas AS INT,
	@partidasEmpatadas AS INT,
	@totalTiempoJugado AS INT,
	@partidasJugadas AS INT
AS
BEGIN
	INSERT INTO Jugador (Nombre,Apellido,Usuario,Clave,PartidasGanadas,PartidasPerdidas,PartidasEmpatadas,TotalTiempoJugado,PartidasJugadas) VALUES
	(@nombre,@apellido,@usuario,@clave,@partidasGanadas,@partidasPerdidas,@partidasEmpatadas,@totalTiempoJugado,@partidasJugadas)
END

GO
/****** Object:  StoredProcedure [dbo].[EditUsuario]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditUsuario]
	@id AS INT,
	@partidasGanadas AS INT,
	@partidasPerdidas AS INT,
	@partidasEmpatadas AS INT,
	@totalTiempoJugado AS INT,
	@partidasJugadas AS INT
AS
BEGIN
	UPDATE Jugador SET PartidasGanadas=@partidasGanadas,PartidasPerdidas=@partidasPerdidas,PartidasEmpatadas=@partidasEmpatadas,TotalTiempoJugado=@totalTiempoJugado,PartidasJugadas=@partidasJugadas
	WHERE Id=@id
END

GO
/****** Object:  StoredProcedure [dbo].[FindPlayerByCredentials]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FindPlayerByCredentials]
	@usuario AS VARCHAR(50),
	@clave AS VARCHAR(50)
AS
BEGIN
	SELECT Id,Nombre,Apellido,Usuario,Clave,PartidasGanadas,PartidasPerdidas,PartidasEmpatadas,TotalTiempoJugado,PartidasJugadas FROM Jugador WHERE Usuario=@usuario AND Clave=@clave
END

GO
/****** Object:  StoredProcedure [dbo].[FindPlayerByUser]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FindPlayerByUser]
	@usuario AS VARCHAR(50),
	@clave AS VARCHAR(50)
AS
BEGIN
	SELECT COUNT(*) AS CANTIDAD FROM Jugador WHERE Usuario=@usuario AND Clave=@clave
END

GO
/****** Object:  StoredProcedure [dbo].[GetBitacora]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBitacora]
AS
BEGIN
	SELECT b.FechaHora,t.Nombre,j.Usuario
	FROM Bitacora b
	INNER JOIN Jugador j ON j.Id=b.IdJugador
	INNER JOIN TipoBitacoraItem t ON t.Id=b.IdTipoBitacoraItem
	ORDER BY b.FechaHora DESC
END

GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUsers]
AS
BEGIN
	SELECT Id,Nombre,Apellido,Usuario,Clave,PartidasGanadas,PartidasPerdidas,PartidasEmpatadas,TotalTiempoJugado,PartidasJugadas FROM Jugador ORDER BY Apellido,Nombre
END

GO
/****** Object:  StoredProcedure [dbo].[RemoveJugador]    Script Date: 17/11/2020 10:53:58 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RemoveJugador]
	@id AS INT
AS
BEGIN
	DELETE FROM Jugador WHERE Id=@id
END

GO
